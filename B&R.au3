#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=img\bkp1.ico
#AutoIt3Wrapper_Outfile=B&R.exe
#AutoIt3Wrapper_Outfile_x64=B&R.exe
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****


#Region################Comments######################
#cs##########################
#   Backup Solution			#
#   FireFox Backup 			#
#   Email Backup			#
#   Documents Folder Backup #
#ce##########################
#EndRegion#############End Comments##################

#Region################GUI Includes##################
	#include <GuiStatusBar.au3>
	#include <EditConstants.au3>
	#include <TrayConstants.au3>
	#include <GUIConstantsEx.au3>
	#include <ButtonConstants.au3>
	#include <WindowsConstants.au3>
#EndRegion#############End GUI Includes##############

#Region################Includes######################
	#include <File.au3>
	#include <Array.au3>
	#include <String.au3>
	#include <FileConstants.au3>
	#include <MsgBoxConstants.au3>
#EndRegion#############End Includes##################

#Region################GUI Variables#################
	Global	$GUI, $nMsg, $tMsg, _
			$File_menu, $SettingsFile, $SaveFolder, $Exititem, _
			$CheckBox_Browsers, $Checkbox_Docs, $CheckBox_Email, $Checkbox_Customs, _
			$Button_Dir, $Button_Execute

#EndRegion#############End GUI Variables#############

#Region################Variables#####################
	Global	$silent = True, $settingsINI = @ScriptDir&'\settings.ini', $saveDest, $Email = False, _
			$aParts[2] = [60,90], $aText = 'Status', _
			$tOpen, $tExit, $tSettings, $tSaveFolder, $tSave, $tSaveBrowser, $tSaveCustoms, $tSaveDocs, $tSaveEmail, _
			$StatusBar, $verbose = False

	Const	$log = @ScriptDir&'\B&R.log'

#Region################End Variables#################

#Region################File Install##################
DirCreate(@ScriptDir&"\img")
FileInstall(".\img\bkp1.ico", @ScriptDir&"\img\bkp1.ico", 1)
#Region################End File Install##############

_Log("Set Tray Mode")
Opt("TrayMenuMode", 3)
Opt("MustDeclareVars", 1)

_Main()

Func _Main()
	Local	$cmd

	_Log("Check for INI file.")
	If Not FileExists($settingsINI) Then
		_Log("INI file not detected. Creating file")
		IniWriteSection($settingsINI, "Save Destination", "Dest="&@ScriptDir&"\Backups")
		_Log("File created at: "&$settingsINI&".")
		_Log("Setting save destination to: "&@ScriptDir&"\Backups.")
		IniWriteSection($settingsINI, "Customs", "")
		IniWriteSection($settingsINI, "Default Save", "Default=/browsers|/docs|/customs")
		_Log("Default save configuration is: /browsers|/docs|/custom.")
	Else
		_Log("Settings ini file found: "&$settingsINI)
	EndIf

	If Not FileExists(@ScriptDir&"\Backups\") then
		_Log("Creating backup directory at: "&@ScriptDir&"\Backups.")
		DirCreate(@ScriptDir&"\Backups\")
	EndIf

	$saveDest = IniRead($settingsINI, "Save Destination", "Dest", "?")

	If $CmdLine[0] = 0 then
		_Log("Silent mode false. Activating main GUI.")
		$silent = False
		_MainGUI()
	Else
		_Log("Entering Silent mode.")
		_Log("Raw CMDLine: "&$CmdLineRaw)
		If StringInStr($CmdLineRaw, 'email') Then $Email = True
		For $x = 1 to $CmdLine[0]
			$cmd = $CmdLine[$x]
			_Log("Switch found: "&$cmd)

			If StringInStr($cmd, "/") Then
				StringReplace($cmd, "/", "")
				_Log("Command cleaned: "&$cmd&".")
			ElseIf StringInStr($cmd, "-") Then
				StringReplace($cmd, "-", "")
				_Log("Command cleaned: "&$cmd&".")
			ElseIf StringInStr($cmd, "\") Then
				StringReplace($cmd, "\", "")
				_Log("Command cleaned: "&$cmd&".")
			EndIf

			Switch StringLower($cmd)
				Case "browsers"
					_Log("Silent browsers backup activated.")
					_Execute("browsers")
				Case "customs"
					_Log("Silent customs backup activated.")
					_Execute("customs")
				Case "docs"
					If Not $Email Then
						_Log("Silent documents backup activated.")
						_Execute("docs")
					EndIf
				case "email"
					_Log("Silent email backup activated.")
					$Email = True
					_Execute("email")
				Case Else
	 				_Log("Silent - Unknown execute - error.")
			EndSwitch
		Next
		$Email = False
	EndIf
EndFunc

Func _MainGUI()
	;TrayMenu
	$tOpen = TrayCreateMenu("Open")
	$tSettings = TrayCreateItem("Settings File", $tOpen)
	$tSaveFolder = TrayCreateItem("Backup Folder", $tOpen)
	;New TrayMenu
	$tSave = TrayCreateMenu("Backup")
	$tSaveEmail = TrayCreateItem("Email", $tSave)
	$tSaveDocs = TrayCreateItem("Docs", $tSave)
	$tSaveBrowser = TrayCreateItem("Browser", $tSave)
	$tSaveCustoms = TrayCreateItem("Customs", $tSave)
	$tExit = TrayCreateItem("Exit")
	TraySetState($TRAY_ICONSTATE_SHOW)

	#Region ### START Koda GUI section ### Form=c:\users\djthornton\dropbox\code\backup\backup gui.kxf
	$GUI = GUICreate("B&R", 162, 135, 736, 297)
	GUISetIcon('.\img\bkp1.ico')
	;FileMenu
	$File_menu = GUICtrlCreateMenu("&File")
	$SettingsFile = GUICtrlCreateMenuItem("Settings File", $File_menu)
	$SaveFolder = GUICtrlCreateMenuItem("Save Folder", $File_menu)
	$Exititem = GUICtrlCreateMenuItem("Exit", $File_menu)
	GUICtrlSetState(-1, $GUI_DEFBUTTON)
	GUICtrlSetState($File_menu, @SW_SHOW)

	;Status
	$StatusBar = _GUICtrlStatusBar_Create($GUI, $aParts, $aText)
	_GUICtrlStatusBar_SetText($StatusBar, 'Ready', 1)

	;CheckBoxs
	$Checkbox_Email = GUICtrlCreateCheckbox("Email", 8, 6, 49, 17)
	$Checkbox_Docs = GUICtrlCreateCheckbox("Documents", 86, 6, 73, 17)
	$Checkbox_Browsers = GUICtrlCreateCheckbox("Browsers", 8, 30, 65, 17)
	$Checkbox_Customs = GUICtrlCreateCheckbox("Customs", 86, 30, 65, 17)

	;Buttons
	$Button_Execute = GUICtrlCreateButton("Execute", 32, 58, 97, 25)
	GUISetState(@SW_SHOW)
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###


While 1
	$nMsg = GUIGetMsg()
	$tMsg = TrayGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit
		Case $Button_Execute
			_Execute()
		Case $SettingsFile
			_SettingsFile()
		Case $SaveFolder
			_SaveFolder()
		Case $Exititem
			Exit
	EndSwitch

	Switch $tMsg
		Case $tExit
			Exit
		Case $tSettings
			_SettingsFile()
		Case $tSaveFolder
			_SaveFolder()
		Case $tSaveBrowser
			_Execute("browsers")
		Case $tSaveCustoms
			_Execute("customs")
		Case $tSaveDocs
			_Execute("docs")
		Case $tSaveEmail
			_Execute("email")
	EndSwitch
	Sleep(25)
WEnd

EndFunc

Func _SettingsFile()
	_Log("Opening settings file: "&@ScriptDir&"\settings.ini.")
	_GUICtrlStatusBar_SetText($StatusBar, 'Working...', 1)
	ShellExecute(@ScriptDir&'\settings.ini', '', '', 'open')
	_GUICtrlStatusBar_SetText($StatusBar, 'Finished', 1)
EndFunc

Func _SaveFolder()
	_Log("Opening save folder: "&$saveDest)
	_GUICtrlStatusBar_SetText($StatusBar, 'Working...', 1)
	ShellExecute($saveDest, '', '', 'open')
	_GUICtrlStatusBar_SetText($StatusBar, 'Finished', 1)
EndFunc

Func _Execute($sExecute = '')
	Local	$cEmail, $cDocs, $cBrowsers, $cCustom, $aExecutes[4][2]

	_GUICtrlStatusBar_SetText($StatusBar, 'Working...', 1)

	If $sExecute Then
		_Log("Silent Execute")
		Switch $sExecute
			Case "docs"
				_Log("Silent - docs")
				_Docs()
			Case "browsers"
				_Log("Silent - browser")
				_Browsers()
			Case "customs"
				_Log("Silent - customs")
				_Customs()
			Case "email"
				_Log("Silent - email")
				$Email = True
				_Docs()
			Case Else
				_Log("Silent - Unknown execute - error")
				;log
		EndSwitch
	Else
		_Log("Vocal Execute")

		$cDocs = BitAND(GUICtrlRead($Checkbox_Docs), $GUI_CHECKED) = $GUI_CHECKED AND $Email = False
		IF $cDocs Then _Log("Docs is checked.")
		$cBrowsers = BitAND(GUICtrlRead($Checkbox_Browsers), $GUI_CHECKED) = $GUI_CHECKED
		IF $cBrowsers Then _Log("Browsers is checked.")
		$cCustom = BitAND(GUICtrlRead($Checkbox_Customs), $GUI_CHECKED) = $GUI_CHECKED
		IF $cCustom Then _Log("Custom is checked.")
		$cEmail = BitAND(GUICtrlRead($Checkbox_Email), $GUI_CHECKED) = $GUI_CHECKED
		IF $cEmail Then _Log("Email is checked.")

		$aExecutes[0][0] = 'docs'
		$aExecutes[0][1] = $cDocs
		$aExecutes[1][0] = 'browsers'
		$aExecutes[1][1] = $cBrowsers
		$aExecutes[2][0] = 'customs'
		$aExecutes[2][1] = $cCustom
		$aExecutes[3][0] = 'email'
		$aExecutes[3][1] = $cEmail

;~ 		_ArrayDisplay($aExecutes,'aExecutes')

		For $e = 0 to UBound($aExecutes) - 1
			Select
				Case $aExecutes[$e][0] = "docs" AND $aExecutes[$e][1] = True
					_Log("Vocal - docs")
					_Docs()
				Case $aExecutes[$e][0] = "browsers" AND $aExecutes[$e][1] = True
					_Log("Vocal - browser")
					_Browsers()
				Case $aExecutes[$e][0] = "customs" AND $aExecutes[$e][1] = True
					_Log("Vocal - customs")
					_Customs()
				Case $aExecutes[$e][0] = "email" AND $aExecutes[$e][1] = True
					_Log("Vocal - email")
					$Email = True
					_Docs()
				Case $aExecutes[$e][1] = False
					_Log($aExecutes[$e][0] & " not checked.")
				Case Else
					_Log("Vocal - Unknown execute - error")
			EndSelect
		Next
		#cs
		If $cEmail then
			_Log("Vocal - email")
			$Email = True
			_Docs()
		ElseIf $cDocs then
			_Log("Vocal - docs")
			_Docs()
			$Email = False
		ElseIf $cBrowsers then
			_Log("Vocal - browsers")
			_Browsers()
		ElseIf $cCustom then
			_Log("Vocal - custom")
			_Customs()
		Else
			If Not $silent Then MsgBox(16, 'No Selection', 'Please make a selection first.')
			_Log("Vocal - No Selection - error")
			Return
		EndIf
		#ce


	EndIf

	_GUICtrlStatusBar_SetText($StatusBar, 'Finished', 1)
	_Log("Backups Complete.")

EndFunc

Func _Browsers()
	Local 	$aBrowsers[4][2]

	$aBrowsers[0][0] = 'FireFox'
	$aBrowsers[0][1] = @AppDataDir&"\Mozilla\Firefox\Profiles" ;All folder in this directory need to be backed up.
	$aBrowsers[1][0] = 'Chrome'
	$aBrowsers[1][1] = @LocalAppDataDir&"\Google\Chrome" ;Default folder needs to be backed up.
	$aBrowsers[2][0] = 'Edge'
	$aBrowsers[2][1] = @LocalAppDataDir&"\Packages\Microsoft.MicrosoftEdge_8wekyb3d8bbwe\AC\MicrosoftEdge\User\Default"  ;Default folder needs to be backed up.
	$aBrowsers[3][0] = 'IE Favorite'
	$aBrowsers[3][1] = @UserProfileDir&'\Favorites' ;Backups Favorites folder.

	For $b = 0 to 3
		If FileExists($aBrowsers[$b][1]) Then
			_Log("Backing up: "&$aBrowsers[$b][1])
			_Copy($aBrowsers[$b][1], $saveDest&'\'&$aBrowsers[$b][0])
		EndIf
	Next

	_Log("Browser backup complete.")

EndFunc

Func _Customs()
	Local	$aCustomSRC, $path, $customName

	$aCustomSRC = IniReadSection($settingsINI, "Customs")
	_Log("$aCustomSRC = "&$aCustomSRC)
	_FileWriteFromArray(@ScriptDir&'\Debug custom array.txt', $aCustomSRC)
	If Not IsArray($aCustomSRC) Then
		_Log("No customs found.")
		If Not $silent Then MsgBox(16,'No Customs', "No custom paths are set in the settings.ini file.")
		Return
	EndIf

	For $p = 1 To $aCustomSRC[0][0]
		$customName = $aCustomSRC[$p][0]
		$path = $aCustomSRC[$p][1]
		_Log("Custom path = "&$path)

		If $path = '' Then
			_Log("Custom path could not be determined from settings ini file for custom: "&$customName&".")
			If Not $silent Then MsgBox(16,'Error', 'One or more custom settings has an incorrectly configured path.')
			ContinueLoop
		EndIf

		_Copy($path, $saveDest&'\Custom')

	Next
	_Log("Custom backup complete.")

EndFunc

Func _Docs()
	Local	$Docs = @MyDocumentsDir ;Backup everything in this folder.

	If $Email = True Then
		_Log("Email parameter is True.")
		_Email()
	EndIf

	_Copy($Docs, $saveDest)
	_Log("Documents backup complete.")

EndFunc

Func _Email()
	Local	$response

	If ProcessExists('OUTLOOK.EXE') Then
		_Log("Outlook.exe is currently running. Prompting to close")
		$response = MsgBox(65,'Caution', 'A backup of outlook has been scheduled. To proceed Outlook.exe must be closed. Please do so before continuing. If you proceed outlook will be closed for you.'&@CRLF&@CRLF& _
								'Thank you!')
		If $response = 2 Then
			MsgBox(64, 'Note', 'Outlook files will be skipped.')
			Return False
		EndIf
		_Log("Closing Outlook.exe")
		If ProcessClose("OUTLOOK.EXE") Then
			_Log("Outlook successfully closed.")
		Else
			_Log("Outlook could not be closed.")
		EndIf
	EndIf
EndFunc

Func _DirSplit($directory)
	Local	$aSplit

	_Log("Determining source folder name.")
	$aSplit = StringSplit($directory,'\')
	If IsArray($aSplit) then
		_Log("Source folder name found: "&$aSplit[$aSplit[0]])
		Return $aSplit[$aSplit[0]]
	EndIf

EndFunc

Func _Copy($src, $dest)
	Local	$dir = True, $response, $attrib, $fName

	$attrib = FileGetAttrib($src)

	_Log("File attributes: "& $attrib)

	If Not StringInStr($attrib, 'D') Then
		_Log("Source is not a folder. Configuring for file copy.")
		$dir = False
	Else
		_Log("Source is a folder. Configuring for directory copy.")
	EndIf

	If Not $dir Then
		_Log("File copy: "&$src&" ---> "&$dest&".")
		$response = FileCopy($src, $dest, 9)
		_Log("File copy response: "&$response&".")
	Else
		$fName = _DirSplit($src)
		$dest &= '\'&$fName
		_Log("Create Direcotory for: "&$dest&".")
		_Log("Directory create return: "&DirCreate($dest))
		_Log("Directory copy: "&$src&" ---> "&$dest&".")
		$response = DirCopy($src, $dest, 1)
		If $response Then
			_Log("Successfully copied "&$fName)
			_Log("Directory copy response: "&$response&".")
		EndIf
	EndIf

EndFunc

Func _Log($Status, $force = '')
	ConsoleWrite($status&@CRLF)
	_FileWriteLog($log, $status)

EndFunc

Func _Exit()

	Exit

EndFunc


